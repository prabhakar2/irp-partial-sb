/*
    * @ Class Name          :       BackupTriggerHandler
    * @ Description         :       Handler Class for BackupTrigger
    * @ Created By          :       Prabhakar Joshi   
    * @ CreatedDate         :       23-Sept-2020
    * @ Last Modified By    :       Shivangi Srivastava
*/

public class BackupTriggerHandler {
    
    /* @ Method to call on Before Insert event. */
    public void beforeInsert(List<Backup__c> triggerNew){
        /* @Calling method to populate payment if no-payment checkbox is true.*/
        this.populatePaymentRecord(triggerNew);
    }
    
    /* @ Method to call on Before Update event. */
    /* @ Added by Shivangi Srivastava on 5-Oct-2020. */
    public void beforeUpdate(List<Backup__c> triggerNew, Map<Id,Backup__c> oldMap ){
        List<Backup__c> backupListToUpdate = new List<Backup__c>();
        for(Backup__c bckup : triggerNew){
            if(bckup.No_Payment__c != oldMap.get(bckup.Id).No_Payment__c || bckup.Payment__c != oldMap.get(bckup.Id).Payment__c){
                backupListToUpdate.add(bckup);
            }
        }
        if(!backupListToUpdate.isEmpty()){
            /* @Calling method to update no-payment checkbox and payment field.*/
            this.populatePaymentRecord(backupListToUpdate);
        }
    }
    
    /** Method Calling on after Insert event from BackupTrigger. */
    public void afterInsert(List<Backup__c> triggerNew){
        this.calculateBackupAmountOnPayment(triggerNew,NULL);
    }
    
    /** Method Calling on after Update event from BackupTrigger. */
    public void afterUpdate(List<Backup__c> triggerNew, Map<Id,Backup__c> oldMap){
        List<Backup__c> backupList = new List<Backup__c>();
        Set<Id> oldParentIds = new Set<Id>();
        for(Backup__c bckup : triggerNew){
            if(bckup.Payment__c != oldMap.get(bckup.Id).Payment__c){
                oldParentIds.add(oldMap.get(bckup.Id).Payment__c);
                backupList.add(bckup);
            }
            else if(bckup.Balance_Adjustment_Amount__c != oldMap.get(bckup.Id).Balance_Adjustment_Amount__c
               || bckup.Back_up_Amount_Received__c != oldMap.get(bckup.Id).Back_up_Amount_Received__c
               || bckup.Direct_Payment_Amount__c != oldMap.get(bckup.Id).Direct_Payment_Amount__c){
                   backupList.add(bckup);
               }
        }
        if(!backupList.isEmpty()){
            this.calculateBackupAmountOnPayment(backupList,oldParentIds);
        }
    }

    /** Method Calling on after Delete event from BackupTrigger. */
    public void afterDelete(List<Backup__c> triggerOld){
        this.calculateBackupAmountOnPayment(triggerOld,NULL);
    }

    /** Method Calling on after Undelete event from BackupTrigger. */
    public void afterUndelete(List<Backup__c> triggerNew){
        this.calculateBackupAmountOnPayment(triggerNew,NULL);
    }
    
    /* @ Method definition to get the record name if no-payment checkbox is true*/
    /* @ Added by Shivangi Srivastava on 5-Oct-2020. */
    private void populatePaymentRecord(List<Backup__c> triggerNew){
        Id noPaymentRecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('No Payment').getRecordTypeId();
        
        List<Payments__c> paymentList = [SELECT id FROM Payments__c WHERE recordTypeId =: noPaymentRecordTypeId LIMIT 1];
        for(Backup__c bk : triggerNew){
            if(bk.No_Payment__c == true){
                bk.Payment__c = paymentList[0].Id;
            }else if(bk.Payment__c == paymentList[0].Id){
                bk.No_Payment__c = true;
            }
        }
    }

    /** Method definition to calculate sum of amounts of backups to be populated on No-Payment */
    /** Calling from afterInsert,afterUpdate,afterDelete and afterUndelete methods. */
    /** Added by Prabhakar Joshi on 23-Oct-2020. */
    private void calculateBackupAmountOnPayment(List<Backup__c> recordList,Set<Id> oldParentIds){
        Map<Id,Decimal> paymentIdToAmountMap = new Map<Id,Decimal>();
        for(Backup__c bk : recordList){
            if(bk.Payment__c == NULL){
                continue;
            }
            paymentIdToAmountMap.put(bk.Payment__c,0);
        }

        if(paymentIdToAmountMap.keySet().isEmpty()){
            return;
        }

        if(oldParentIds != NULL && !oldParentIds.isEmpty()){
            for(Id oldPayId : oldParentIds){
                if(!paymentIdToAmountMap.containsKey(oldPayId)){
                    paymentIdToAmountMap.put(oldPayId,0);
                }
            }
        }

        for(AggregateResult agg : [SELECT Payment__c,SUM(Direct_Payment_Amount__c) directAmt, 
                                   SUM(Back_up_Amount_Received__c) reclAmt, SUM(Balance_Adjustment_Amount__c) balAdjAmt
                                   FROM Backup__c
                                   WHERE Payment__c IN :paymentIdToAmountMap.keySet()
                                   GROUP BY Payment__c]){

                                       Decimal directAmt = (Decimal)agg.get('directAmt') != NULL ? (Decimal)agg.get('directAmt') : 0;
                                       Decimal reclAmt = (Decimal)agg.get('reclAmt') != NULL ? (Decimal)agg.get('reclAmt') : 0;
                                       Decimal balAdjAmt = (Decimal)agg.get('balAdjAmt') != NULL ? (Decimal)agg.get('balAdjAmt') : 0;
                                       
                                       Decimal totalAmt = directAmt + reclAmt + balAdjAmt; 
                                       paymentIdToAmountMap.put((Id)agg.get('Payment__c'),totalAmt);
                                   }
        List<Payments__c> payListToUpdate = new List<Payments__c>();
        for(Id payId : paymentIdToAmountMap.keySet()){
            Payments__c pay = new Payments__c();
            pay.Id = payId;
            pay.Total_Backup_Amount__c = paymentIdToAmountMap.get(payId);
            payListToUpdate.add(pay);
        }
        if(!payListToUpdate.isEmpty()){
            update payListToUpdate;
        }
    }
}
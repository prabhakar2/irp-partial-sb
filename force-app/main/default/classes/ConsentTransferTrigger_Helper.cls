public class ConsentTransferTrigger_Helper 
{
        public void updateInstallmentConsentTransfer(List<Consent_Transfer__c> triggerNew)
        {
         //   system.debug('triggerNew-->'+triggerNew);
    	//		system.debug('deal id-->'+triggerNew[0].deal__c);
            
            Map<Id, Consent_Transfer__c> changed = new Map<Id, Consent_Transfer__c>();
            list<installment__c> insList = new list<installment__c>();
            
            for (Consent_Transfer__c ct: triggerNew) 
            {
                changed.put(ct.deal__c,ct);
          //      system.debug('consentIds'+triggerNew[0].deal__c);
            }
            for(installment__c ins :[select id,deal_name__c from installment__c where deal_name__c IN : changed.keySet()])
            {
                Consent_Transfer__c cts = changed.get(ins.deal_name__c);
                ins.Accuracy_Rep__c = cts.Accuracy_Rep__c;
                ins.Phone_Data_Existence__c = cts.Phone_Data_Existence__c;
                ins.Upfront_Phone_Number_s_Provided__c = cts.Upfront_Phone_Number_s_Provided__c;
                ins.Upfront_Phone_Number_s_Provided_Type__c = cts.Upfront_Phone_Number_s_Provided_Type__c;
                ins.Additional_Numbers__c = cts.Additional_Numbers__c;
                ins.Additional_Number_s_Type__c = cts.Additional_Number_s_Type__c;
                ins.Phone_Notes__c = cts.Phone_Notes__c;
                ins.Phone_Validity_Code_PVC_Availability__c = cts.Phone_Validity_Code_PVC_Availability__c;
                ins.Further_Information__c = cts.Further_Information__c;
                ins.Contract__c = cts.Contract__c;
                ins.Practice__c = cts.Practice__c;
                ins.Go_Forward_Confidence__c = cts.Go_Forward_Confidence__c;
                ins.Have_Attachments_Been_Uploaded__c = cts.Have_Attachments_Been_Uploaded__c;
                insList.add(ins);
            }
            if (!insList.isEmpty()) {
            update insList;
        }
        } 
    
    public void updateInstallmentConsentCHA(List<Consent_Transfer__c> triggerNew , Map<Id,Consent_Transfer__c> oldMap)
    {
        Map<Id, Consent_Transfer__c> changed = new Map<Id, Consent_Transfer__c>();
        List<Installment__c> instList = new List<Installment__c>();
        
        for (Consent_Transfer__c ct: triggerNew) {
            Consent_Transfer__c oldCT = oldMap.get(ct.Id);
            if (ct.Arbitration__c != oldCT.Arbitration__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Assignment__c != oldCT.Assignment__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Communication__c != oldCT.Communication__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Arbitration_Comments__c != oldCT.Arbitration_Comments__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Assignment_Comments__c != oldCT.Assignment_Comments__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Communication_Comments__c != oldCT.Communication_Comments__c ) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Date_T_C_Language_Sent_for_Legal_Review__c != oldCT.Date_T_C_Language_Sent_for_Legal_Review__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Date_Legal_Completes_T_C_Review__c != oldCT.Date_Legal_Completes_T_C_Review__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if (ct.Legal_Notes__c != oldCT.Legal_Notes__c) 
            {
                changed.put(ct.deal__c, ct);
            }
            if(ct.Has_Consent_Transfer_Been_Approved__c != oldCT.Has_Consent_Transfer_Been_Approved__c)
            {
                changed.put(ct.deal__c,ct);
            }
            if(ct.ESC_Consent_Transfer_Proposal_Review__c != oldCT.ESC_Consent_Transfer_Proposal_Review__c)
            {
                changed.put(ct.deal__c,ct);
            }
            if(ct.Consent_Transfer_Approved_by_ESC__c != oldCT.Consent_Transfer_Approved_by_ESC__c)
            {
                changed.put(ct.deal__c,ct);
            }
            if(ct.Consent_Transfer_Deployment_Date__c != oldCT.Consent_Transfer_Deployment_Date__c)
            {
                changed.put(ct.deal__c,ct);
            }
        }
        for (Installment__c ins: [select id,deal_name__c from Installment__c where deal_name__c IN : changed.keySet()]) 
        {
            Consent_Transfer__c cts = changed.get(ins.deal_name__c);
            ins.Arbitration__c = cts.Arbitration__c;
            ins.Assignment__c = cts.Assignment__c;
            ins.Communication__c = cts.Communication__c;
            ins.Arbitration_Comments__c = cts.Arbitration_Comments__c;
            ins.Assignment_Comments__c = cts.Assignment_Comments__c;
            ins.Communication_Comments__c = cts.Communication_Comments__c;
            ins.Legal_Notes__c = cts.Legal_Notes__c;
            ins.Date_T_C_Language_Sent_for_Legal_Review__c = cts.Date_T_C_Language_Sent_for_Legal_Review__c;
            ins.Date_Legal_Completes_T_C_Review__c = cts.Date_Legal_Completes_T_C_Review__c;
            ins.Has_Consent_Transfer_Been_Approved__c= cts.Has_Consent_Transfer_Been_Approved__c;
            ins.ESC_Consent_Transfer_Proposal_Review__c= cts.ESC_Consent_Transfer_Proposal_Review__c;
            ins.Consent_Transfer_Approved_by_ESC__c = cts.Consent_Transfer_Approved_by_ESC__c;
            ins.Consent_Transfer_Deployment_Date__c = cts.Consent_Transfer_Deployment_Date__c;
            instList.add(ins);
        }
        if (!instList.isEmpty()) {
            update instList;
        }
    }
}
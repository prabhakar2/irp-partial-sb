public class DisbursementController {
    public Id paymentId{
        get;
        private set;
    }
    public Disbursement__c disbObj {
        get;
        set;
    }

    public DisbursementController(){
        disbObj = new Disbursement__c();
        paymentId = ApexPages.currentPage().getParameters().get('id');
        if(paymentId != NULL){
            disbObj.Payment__c = paymentId;
        }

    }
}
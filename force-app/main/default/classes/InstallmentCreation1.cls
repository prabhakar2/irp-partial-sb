public class InstallmentCreation1 
{
    /******************************Method designed to create installments*****************************************/
	@AuraEnabled
    public static void saveInstallment(String dealId)
    {
        String msg ='';
        try{
        List<Installment__c> insList = new List<Installment__c>();
        
        Deals__c d =[select id,of_Installments__c,of_Months__c,of_Installments_to_be_Created__c,Legal_Selling_Entity_2__c,Bid_Status__c,
                       	Avg_Age_at_Valuation__c,Start_Date__c,Expected_Monthly_Face_Value__c,Expected_Monthly_of_Accounts__c,BK_Accounts__c,
                       	Market_Share_1__c,Create_Installments__c,Market_Share_2__c,Asset_class__c,Broker_fee__c,Includes_Media_Amt__c,Legal_Selling_Entity1__c,
                       	Media_Cost__c,Product_Family_New__r.Name from deals__c where id =: dealId];
            
            d.Id = dealId;
            d.Create_Installments__c = true;
            update d;
    system.debug('>>>>>>>>'+d);
        date startDate = d.Start_Date__c;
        String stage = d.Bid_Status__c;
        String productName = d.Product_Family_New__r.Name;
        
        if(string.isBlank(d.Legal_Selling_Entity_2__c) && d.of_Installments__c == 0)
        {
            	
            for(integer i=0; i < d.of_Months__c;i++)
            {
             	Installment__c ins = new Installment__c();
                ins.Avg_Age__c = d.Avg_Age_at_Valuation__c;
                ins.Deal_Name__c = d.Id;
                ins.Est_Close_Date__c = startDate.addMonths(i);
                ins.Face_Value__c = d.Expected_Monthly_Face_Value__c * d.Market_Share_1__c ;
                ins.Purchase_Rate__c = 0;
                ins.of_Accts__c = d.Expected_Monthly_of_Accounts__c * d.Market_Share_1__c ;
                ins.Asset_Class__c = d.Asset_Class__c;
                ins.Bid_Rate__c = 0;
                ins.BK_Data__c = d.BK_Accounts__c;
                ins.Broker_Fee__c = d.Broker_Fee__c ;
                ins.Close_Date_Confirmed__c = 'No';
                ins.Legal_Selling_Entity_1__c = d.Legal_Selling_Entity1__c;
                ins.Media_Cost_Deal__c = d.Media_Cost__c ;
                ins.Name = 'test';
                ins.Number_in_Flow__c = string.valueOf(i+1);
                ins.Purchase_Amt_Confirmed__c ='No';
                ins.Update__c = 'X';
                ins.X1st_2nd_LSE__c ='First';
                
                Date dt = ins.Est_Close_Date__c;
                ins.Date_File_Expected__c = dt.addDays(-5);
                
                insList.add(ins);   
            }
            	if(!insList.isEmpty())
                {insert insList;}
        }
        
        else if(d.Legal_Selling_Entity_2__c != null && d.of_Installments__c == 0)
        {
            	
            Map<Integer,String> monthNameMap1=new Map<Integer, String>{1 =>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May',                         
    			6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep',10=>'Oct', 11=>'Nov', 12=>'Dec'};
            
            for(integer i=0 ; i < d.of_Months__c ; i++)
            {
                Installment__c ins = new Installment__c();
                ins.Avg_Age__c = d.Avg_Age_at_Valuation__c;
                ins.Deal_Name__c = d.Id;
                ins.Est_Close_Date__c = startDate.addMonths(i);
                ins.Face_Value__c = d.Expected_Monthly_Face_Value__c * d.Market_Share_1__c ;
                ins.Purchase_Rate__c = 0;
                ins.of_Accts__c = d.Expected_Monthly_of_Accounts__c * d.Market_Share_1__c ;
                ins.Asset_Class__c = d.Asset_Class__c;
                ins.Bid_Rate__c = 0;
                ins.BK_Data__c = d.BK_Accounts__c;
                ins.Broker_Fee__c = d.Broker_Fee__c ;
                ins.Close_Date_Confirmed__c = 'No';
                ins.Legal_Selling_Entity_1__c = d.Legal_Selling_Entity1__c;
                ins.Media_Cost_Deal__c = d.Media_Cost__c ;
                ins.Number_in_Flow__c = string.valueOf(i+1);
                ins.X1st_2nd_LSE__c ='First';
                if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Citibank, N.A.' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = d.Product_Family_New__r.Name+' '+'CBNA'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Citibank, N.A.' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = d.Product_Family_New__r.Name+' '+'CBNA'+' '+'Bulk'+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
               else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Department Stores National Bank' && d.of_Installments_to_be_Created__c > 1)
                {
                 ins.Name = d.Product_Family_New__r.Name+' '+'DSNB'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Department Stores National Bank' && d.of_Installments_to_be_Created__c == 1)
                {
                 ins.Name = d.Product_Family_New__r.Name+' '+'DSNB'+' '+'Bulk'+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Capital One Bank (USA), National Association' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = d.Product_Family_New__r.Name+' '+'COBNA'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Capital One Bank (USA), National Association' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = d.Product_Family_New__r.Name+' '+'COBNA'+' '+'Bulk'+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Comenity Capital Bank' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Comenity Capital Bank' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Capital One, National Association' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = d.Product_Family_New__r.Name+' '+'CONA'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Capital One, National Association' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = d.Product_Family_New__r.Name+' '+'CONA'+' '+'Bulk'+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Comenity Bank' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = 'Comenity'+' '+ productName.substringAfter(' ')+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity1__c =='Comenity Bank' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = 'Comenity'+' '+productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap1.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else
                {
                    ins.Name = 'test';
                }
                ins.Update__c = 'X';
                ins.Purchase_Amt_Confirmed__c ='No';
                
                
                Installment__c ins1 = new Installment__c();
                ins1.Avg_Age__c = d.Avg_Age_at_Valuation__c;
                ins1.Deal_Name__c = d.Id;
                ins1.Est_Close_Date__c = startDate.addMonths(i);
                ins1.Face_Value__c = d.Expected_Monthly_Face_Value__c * d.Market_Share_2__c ;
                ins1.Purchase_Rate__c = 0;
                ins1.of_Accts__c = d.Expected_Monthly_of_Accounts__c * d.Market_Share_2__c ;
                ins1.Asset_Class__c = d.Asset_Class__c;
                ins1.Bid_Rate__c = 0;
                ins1.BK_Data__c = d.BK_Accounts__c;
                ins1.Broker_Fee__c = d.Broker_Fee__c ;
                ins1.Close_Date_Confirmed__c = 'No';
                ins1.Legal_Selling_Entity_1__c = d.Legal_Selling_Entity_2__c;
                ins1.Media_Cost_Deal__c = d.Media_Cost__c ;
                ins1.Number_in_Flow__c = string.valueOf(i+1);
                ins1.X1st_2nd_LSE__c ='Second';
                if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Department Stores National Bank' && d.of_Installments_to_be_Created__c > 1)
                {
                 ins1.Name = d.Product_Family_New__r.Name+' '+'DSNB'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Department Stores National Bank' && d.of_Installments_to_be_Created__c == 1)
                {
                 ins1.Name = d.Product_Family_New__r.Name+' '+'DSNB'+' '+'Bulk'+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Capital One Bank (USA), National Association' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = d.Product_Family_New__r.Name+' '+'COBNA'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Capital One Bank (USA), National Association' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = d.Product_Family_New__r.Name+' '+'COBNA'+' '+'Bulk'+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Comenity Capital Bank' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Comenity Capital Bank' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Citibank, N.A.' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = d.Product_Family_New__r.Name+' '+'CBNA'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Citibank, N.A.' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = d.Product_Family_New__r.Name+' '+'CBNA'+' '+'Bulk'+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Capital One, National Association' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = d.Product_Family_New__r.Name+' '+'CONA'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Capital One, National Association' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = d.Product_Family_New__r.Name+' '+'CONA'+' '+'Bulk'+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Comenity Bank' && d.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = 'Comenity'+' '+ productName.substringAfter(' ')+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(d.Legal_Selling_Entity1__c != null && d.Legal_Selling_Entity_2__c!= null &&
                  d.Legal_Selling_Entity_2__c =='Comenity Bank' && d.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = 'Comenity'+' '+productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap1.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else
                {
                    ins1.Name = 'test';
                }
                ins1.Update__c = 'X';
                ins1.Purchase_Amt_Confirmed__c ='No';
                
                Date dt = ins.Est_Close_Date__c;
                ins.Date_File_Expected__c = dt.addDays(-5);
                ins1.Date_File_Expected__c = dt.addDays(-5);
                
                insList.add(ins); 
                insList.add(ins1); 
            }
            if(!insList.isEmpty())
            {insert insList;}     
        }
        
        else if(d.of_Installments__c > 0 && (stage =='Bid Won'|| stage=='Bid Lost'))
      {			
          system.debug('bid won deletion entering');
          		Deals__c de = new Deals__c();
            	de.Id = dealId;
            	de.Create_Installments__c = false;
          		de.Delete_Installments__c = true;
            	update de;
          
          		list<Installment__c> instList = new list<installment__c>();
                for(installment__c ins:[select id,deal_name__c from installment__c where deal_name__c =: dealId])
                {
                    instList.add(ins);
                }
                if(!instList.isEmpty())
                {delete instList;}
          
      }
		      
        /************This method deletes the installment if status is not bid Won/Lost and again creates it****************/
        else if(d.of_Installments__c > 0 && (stage <> 'Bid Won'|| stage <> 'Bid Lost'))
        {	system.debug('entering for deleting');
         	List<Installment__c> insList1 = new List<Installment__c>();
            for(installment__c ins:[select id,deal_name__c from installment__c where deal_name__c =: dealId])
            {
                insList1.add(ins);
            }
            if(!insList1.isEmpty())
            {delete insList1;}
            system.debug('successfully deleted now creating');
         
         Deals__c dea =[select id,of_Installments__c,of_Months__c,of_Installments_to_be_Created__c,Legal_Selling_Entity_2__c,Bid_Status__c,
                       	Avg_Age_at_Valuation__c,Start_Date__c,Expected_Monthly_Face_Value__c,Expected_Monthly_of_Accounts__c,BK_Accounts__c,
                       	Market_Share_1__c,Create_Installments__c,Market_Share_2__c,Asset_class__c,Broker_fee__c,Includes_Media_Amt__c,Legal_Selling_Entity1__c,
                       	Media_Cost__c,Product_Family_New__r.Name from deals__c where id =: dealId];
    
        date startDate1 = dea.Start_Date__c;
         
            if(string.isBlank(dea.Legal_Selling_Entity_2__c) && dea.of_Installments__c == 0)
        {
            
            for(integer i=0; i < d.of_Months__c;i++)
            {
             	Installment__c ins = new Installment__c();
                ins.Avg_Age__c = dea.Avg_Age_at_Valuation__c;
                ins.Deal_Name__c = dea.Id;
                ins.Est_Close_Date__c = startDate1.addMonths(i);
                ins.Face_Value__c = dea.Expected_Monthly_Face_Value__c * dea.Market_Share_1__c ;
                ins.Purchase_Rate__c = 0;
                ins.of_Accts__c = dea.Expected_Monthly_of_Accounts__c * dea.Market_Share_1__c ;
                ins.Asset_Class__c = dea.Asset_Class__c;
                ins.Bid_Rate__c = 0;
                ins.BK_Data__c = dea.BK_Accounts__c;
                ins.Broker_Fee__c = dea.Broker_Fee__c ;
                ins.Close_Date_Confirmed__c = 'No';
                ins.Legal_Selling_Entity_1__c = dea.Legal_Selling_Entity1__c;
                ins.Media_Cost_Deal__c = dea.Media_Cost__c ;
                ins.Name = 'test';
                ins.Update__c = 'X';
                ins.Number_in_Flow__c = string.valueOf(i+1);
                ins.Purchase_Amt_Confirmed__c ='No';
                ins.X1st_2nd_LSE__c ='First';
                
                Date dt = ins.Est_Close_Date__c;
                ins.Date_File_Expected__c = dt.addDays(-5);

                insList.add(ins);   
            }
            	if(!insList.isEmpty())
                {insert insList;}
        }
            else if(dea.Legal_Selling_Entity_2__c != null && dea.of_Installments__c == 0)
        {		system.debug('now creating installments again');
            
         Map<Integer,String> monthNameMap=new Map<Integer, String>{1 =>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May',                         
    			6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep',10=>'Oct', 11=>'Nov', 12=>'Dec'};
         
            for(integer i=0 ; i < d.of_Months__c ; i++)
            {
                Installment__c ins = new Installment__c();
                ins.Avg_Age__c = dea.Avg_Age_at_Valuation__c;
                ins.Deal_Name__c = dea.Id;
                ins.Est_Close_Date__c = startDate1.addMonths(i);
                ins.Face_Value__c = dea.Expected_Monthly_Face_Value__c * dea.Market_Share_1__c ;
                ins.Purchase_Rate__c = 0;
                ins.of_Accts__c = dea.Expected_Monthly_of_Accounts__c * dea.Market_Share_1__c ;
                ins.Asset_Class__c = dea.Asset_Class__c;
                ins.Bid_Rate__c = 0;
                ins.BK_Data__c = dea.BK_Accounts__c;
                ins.Broker_Fee__c = dea.Broker_Fee__c ;
                ins.Close_Date_Confirmed__c = 'No';
                ins.Legal_Selling_Entity_1__c = dea.Legal_Selling_Entity1__c;
                ins.Media_Cost_Deal__c = dea.Media_Cost__c ;
                ins.Number_in_Flow__c = string.valueOf(i+1);
                ins.X1st_2nd_LSE__c ='First';
                
                if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Citibank, N.A.' && dea.of_Installments_to_be_Created__c > 1 && dea.Create_Installments__c == true)
                {
                 ins.Name = dea.Product_Family_New__r.Name+' '+'CBNA'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Citibank, N.A.' && dea.of_Installments_to_be_Created__c == 1 && dea.Create_Installments__c == true)
                {
                 ins.Name = dea.Product_Family_New__r.Name+' '+'CBNA'+' '+'Bulk'+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Capital One, National Association' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = dea.Product_Family_New__r.Name+' '+'CONA'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Capital One, National Association' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = dea.Product_Family_New__r.Name+' '+'CONA'+' '+'Bulk'+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Comenity Bank' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = 'Comenity'+' '+ productName.substringAfter(' ')+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Comenity Bank' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = 'Comenity'+' '+ productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Department Stores National Bank' && dea.of_Installments_to_be_Created__c > 1)
                {
                 ins.Name = dea.Product_Family_New__r.Name+' '+'DSNB'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Department Stores National Bank' && dea.of_Installments_to_be_Created__c == 1)
                {
                 ins.Name = dea.Product_Family_New__r.Name+' '+'DSNB'+' '+'Bulk'+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Capital One Bank (USA), National Association' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = dea.Product_Family_New__r.Name+' '+'COBNA'+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Capital One Bank (USA), National Association' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = dea.Product_Family_New__r.Name+' '+'COBNA'+' '+'Bulk'+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Comenity Capital Bank' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'FF'+ins.Number_in_Flow__c+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity1__c =='Comenity Capital Bank' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap.get(ins.Est_Close_Date__c.Month())+' '+ins.Est_Close_Date__c.Year();  
                }
                else
                {
                ins.Name = 'test';
                }
                ins.Update__c = 'X';
                ins.Purchase_Amt_Confirmed__c ='No';
                
                Installment__c ins1 = new Installment__c();
                ins1.Avg_Age__c = dea.Avg_Age_at_Valuation__c;
                ins1.Deal_Name__c = dea.Id;
                ins1.Est_Close_Date__c = startDate1.addMonths(i);
                ins1.Face_Value__c = dea.Expected_Monthly_Face_Value__c * dea.Market_Share_2__c ;
                ins1.Purchase_Rate__c = 0;
                ins1.of_Accts__c = dea.Expected_Monthly_of_Accounts__c * dea.Market_Share_2__c ;
                ins1.Asset_Class__c = dea.Asset_Class__c;
                ins1.Bid_Rate__c = 0;
                ins1.BK_Data__c = dea.BK_Accounts__c;
                ins1.Broker_Fee__c = dea.Broker_Fee__c ;
                ins1.Close_Date_Confirmed__c = 'No';
                ins1.Legal_Selling_Entity_1__c = dea.Legal_Selling_Entity_2__c;
                ins1.Media_Cost_Deal__c = dea.Media_Cost__c ;
                ins1.Number_in_Flow__c = string.valueOf(i+1);
                ins1.X1st_2nd_LSE__c ='Second';
                if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Department Stores National Bank' && dea.of_Installments_to_be_Created__c > 1 && dea.Create_Installments__c == true)
                {
                 ins1.Name = dea.Product_Family_New__r.Name+' '+'DSNB'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Department Stores National Bank' && dea.of_Installments_to_be_Created__c == 1 && dea.Create_Installments__c == true)
                {
                 ins1.Name = dea.Product_Family_New__r.Name+' '+'DSNB'+' '+'Bulk'+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Capital One Bank (USA), National Association' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = dea.Product_Family_New__r.Name+' '+'COBNA'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Capital One Bank (USA), National Association' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = d.Product_Family_New__r.Name+' '+'COBNA'+' '+'Bulk'+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Comenity Capital Bank' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Comenity Capital Bank' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = 'Comenity Capital'+' '+ productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Citibank, N.A.' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = dea.Product_Family_New__r.Name+' '+'CBNA'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Citibank, N.A.' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = dea.Product_Family_New__r.Name+' '+'CBNA'+' '+'Bulk'+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Capital One, National Association' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = dea.Product_Family_New__r.Name+' '+'CONA'+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Capital One, National Association' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = dea.Product_Family_New__r.Name+' '+'CONA'+' '+'Bulk'+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Comenity Bank' && dea.of_Installments_to_be_Created__c > 1 )
                {
                 ins1.Name = 'Comenity'+' '+ productName.substringAfter(' ')+' '+'FF'+ins1.Number_in_Flow__c+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();
                }
                else if(dea.Legal_Selling_Entity1__c != null && dea.Legal_Selling_Entity_2__c!= null &&
                  dea.Legal_Selling_Entity_2__c =='Comenity Bank' && dea.of_Installments_to_be_Created__c == 1)
                {
                  ins1.Name = 'Comenity'+' '+productName.substringAfter(' ')+' '+'Bulk'+' '+monthNameMap.get(ins1.Est_Close_Date__c.Month())+' '+ins1.Est_Close_Date__c.Year();  
                }
                else
                {
                    ins1.Name = 'test';
                }
                ins1.Update__c = 'X';
                ins1.Purchase_Amt_Confirmed__c ='No';
                
                Date dt = ins.Est_Close_Date__c;
                ins.Date_File_Expected__c = dt.addDays(-5);
                ins1.Date_File_Expected__c = dt.addDays(-5);
                
                insList.add(ins); 
                insList.add(ins1); 
            }
            if(!insList.isEmpty())
            {insert insList;}     
        }
        }
       } 
        catch(DmlException e)
        {for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            throw new AuraHandledException(msg);
             
        }catch(Exception e){
            //throw all other exception message
            throw new AuraHandledException(e.getMessage());
        }
        finally {
        }
    }
}
@isTest
public class InstallmentTrigger_Test {
    @isTest
    static void test1(){
        Trigger_Setting__c ts = new Trigger_Setting__c();
        ts.Name = 'InstallmentTrigger';
        ts.Active__c = true;
        insert ts;
        
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        
        Product_Family__c pf1 = new Product_Family__c();
        pf1.Company_Name__c = acc.Id;
        insert pf1;
        
        Product_Family__c pf2 = new Product_Family__c();
        pf2.Company_Name__c = acc.Id;
        pf2.Parent_Product_Family__c = pf1.Id;
        pf2.Asset_Class__c = 'Credit Card';
        pf2.Paper_Type__c = 'CC01';
        pf2.Agency_Cycle__c = 'All';
        pf2.Product_Type__c = 'Core';
        pf2.Region__c = 'US';
        insert pf2;
        
        Deals__c dl = new Deals__c();
        dl.Name = 'test';
        dl.Company_Name__c = acc.Id;
        dl.Product_Family_New__c = pf2.Id;
        dl.Exp_of_Accts__c = 1234;
        dl.Avg_Age_at_Valuation__c = 1234;
        dl.Exp_Mkt_Clearing_Price__c = 44444;
        dl.Exp_Face_Value__c = 434343;
        dl.Bid_Status__c = 'Forecast';
        insert dl;
        
        Installment__c insOld = new Installment__c();
        insOld.Create_Pool_Port__c = false;
        insOld.Deal_Name__c = dl.Id;
        insOld.Face_Value__c = 212121;
        insOld.of_Accts__c = 323232;
        insOld.Avg_Age__c = 10;
        insOld.Est_Close_Date__c = System.today();
        insOld.Purchase_Rate__c = 5;
        insOld.Portfolio__c = '112233';
        insOld.Portfolio_No__c = 112233;
        insert insOld;
        
        Installment__c ins = new Installment__c();
        ins.Create_Pool_Port__c = false;
        ins.Deal_Name__c = dl.Id;
        ins.Face_Value__c = 212121;
        ins.of_Accts__c = 323232;
        ins.Avg_Age__c = 10;
        ins.Est_Close_Date__c = System.today();
        ins.Purchase_Rate__c = 5;
        insert ins;
        
        ins.Create_Pool_Port__c = true;
        update ins;
    }
}
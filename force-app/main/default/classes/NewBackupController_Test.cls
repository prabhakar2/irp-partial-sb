/*
	* @ Class Name 		    : 	    NewBackupController_Test
 	* @ Description 	    : 	    Test class for NewBackupController
 	* @ Created By 		    : 	    Prabhakar Joshi
    * @ Created Date	    : 	    20-Oct-2020 
*/
@isTest
public with sharing class NewBackupController_Test {
    @TestSetup
    static void makeData(){
        Payments__c pay =new Payments__c();
        pay.RecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('Payments').getRecordTypeId();
        pay.Payment_Type__c = 'Check';
        insert pay;
    }

    @isTest
    static void test1(){
        List<Payments__c> payList = [SELECT Id,Name,recordTypeId,Payment_Type__c FROM Payments__c];
        System.assertEquals(1, payList.size());
        Pagereference pg = Page.NewBackup;
        
        Apexpages.currentPage().getParameters().put('id',payList[0].Id);
        System.assertEquals(payList[0].Id, Apexpages.currentPage().getParameters().get('id'));
        NewBackupController obj = new NewBackupController();
        obj.getRecordTypeInfo();
        obj.handleRTChange();
        obj.continueAfterRTSelect();
        obj.saveNew();
        obj.saveRecord();
    }
}

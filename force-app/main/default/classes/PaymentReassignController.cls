/*
	* @ Class Name 		    : 	    PaymentReassignController
 	* @ Description 	    : 	    Controller for PaymentReassign VF Page.
 	* @ Created By 		    : 	    Prabhakar Joshi
    * @ Created Date	    : 	    13-Oct-2020 
*/
public with sharing class PaymentReassignController {
    /* @ To hold the payment number for search. */
    public String paymentNumber {
        get;
        set;
    }

    /* @ To hold the payment records with checkbox after searching. */
    public List<PaymentWrapper> searchPayList{
        get;
        set;
    }
    
    /* @ To hold the backup records with checkbox. */
    public List<BackupWrapper> backupWrapList{
        get;
        set;
    }

    /* @ To hold the payment record Id getting from URL. */
    public Id currentPayId{
        get;
        set;
    }

    /* @ To hold the payment record which is selected for assigning the backups and batches.  */
    public Payments__c selectedPayRecord{
        get;
        set;
    }

    /* @ Constructor of class */
    public PaymentReassignController() {
        currentPayId = ApexPages.currentPage().getParameters().get('id');
        paymentNumber = '';
        searchPayList = new List<PaymentWrapper>();
        backupWrapList = new List<BackupWrapper>();
    }

    /* @ Method to search the payment number and show the result in the table. */
    /* @ Calling from 'Search' button on the page. */
    public void search(){
        /* @ To hold the recordTypeId of No Payment recordType. */
        Id noPaymentRecordTypeId = Schema.SObjectType.Payments__c.getRecordTypeInfosByName().get('No Payment').getRecordTypeId();
        
        searchPayList = new List<PaymentWrapper>();
        backupWrapList = new List<BackupWrapper>();
        if(String.isNotBlank(paymentNumber)){
            if(paymentNumber.length() >= 6){
                String searchStr = paymentNumber+'%';
                for(Payments__c pay : [SELECT Id,Name,Company_Name__c,Company_Name__r.Name 
                                       FROM Payments__c 
                                       WHERE recordTypeId != :noPaymentRecordTypeId 
                                       AND Name LIKE :searchStr 
                                       AND Company_Name__c != NULL]){
                    searchPayList.add(new PaymentWrapper(false,pay));
                }

                if(searchPayList.isEmpty()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No Payment record found.'));    
                }
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Please enter a valid payment number prefix of 6 digits.'));
            }
        }else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Please enter 6 digits prefix of any payment number.'));
        }
    }/* End of 'search' method. */

    /* @ Method to get the related backup records and show them in a table. */
    /* @ Calling from 'Get Backup' button on the page. */
    public void fetchBackups(){
        backupWrapList = new List<BackupWrapper>();
        selectedPayRecord = new Payments__c();
        for(PaymentWrapper wrp : searchPayList){
            if(wrp.isSelected){
                selectedPayRecord = wrp.payObj;
                break;
            }
        }
        if(selectedPayRecord.Company_Name__c != NULL){
            for(Backup__c bk : [SELECT Id,Name,Company__c,Company__r.Name,Status__c,Payment__c,Payment__r.Name,
                                RecordType.Name,Back_up_Amount_Received__c,CreatedDate 
                                FROM Backup__c 
                                WHERE Payment__c =:currentPayId 
                                AND Company__c =:selectedPayRecord.Company_Name__c]){
                backupWrapList.add(new BackupWrapper(false,bk));
            }
            if(backupWrapList.isEmpty()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'No Backup record found.'));    
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Please select any payment.'));
        }
    }/* End of 'fetchBackups' method. */

    /* @ Method to reassign the no payment backups and batches with the selected payment record. */
    /* @ Calling from Reassign button on the page. */
    public PageReference reassign(){
        List<Backup__c> bkListToUpdate = new List<Backup__c>();
        for(BackupWrapper wrp  : backupWrapList){
            if(wrp.isSelected){
                wrp.backupObj.No_Payment__c = false;
                wrp.backupObj.Payment__c = selectedPayRecord.Id;
                bkListToUpdate.add(wrp.backupObj);
            }
        }
        if(!bkListToUpdate.isEmpty()){
            List<Batches__c> batchListToUpdate = new List<Batches__c>();
            for(Batches__c bt :[SELECT Id,Backup__c,Payment__c from Batches__c WHERE Backup__c IN : bkListToUpdate]){
                bt.Payment__c = selectedPayRecord.Id;
                batchListToUpdate.add(bt);
            }
            try{
                if(!batchListToUpdate.isEmpty()){
                    update batchListToUpdate;
                }
                update bkListToUpdate;
                return new PageReference('/'+currentPayId);
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
            }
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Please select any backup record.'));
        }
        return NULL;
    }/* End of 'reassign' method. */

    /* @ Wrapper class to bind the checkbox with payment record. */
    public class PaymentWrapper{
        public Boolean isSelected{
            get;
            set;
        }
        public Payments__c payObj{
            get;
            set;
        }

        public PaymentWrapper(Boolean isSelected,Payments__c payObj){
            this.isSelected = isSelected;
            this.payObj = payObj;
        }
    }/* End of wrapper class. */

    /* @ Wrapper class to bind the checkbox with backup record. */
    public class BackupWrapper{
        public Boolean isSelected{
            get;
            set;
        }
        public Backup__c backupObj{
            get;
            set;
        }

        public BackupWrapper(Boolean isSelected,Backup__c backupObj){
            this.isSelected = isSelected;
            this.backupObj = backupObj;
        }
    }/* End of wrapper class. */
}/* End of main class. */
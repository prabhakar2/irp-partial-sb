/*
###########################################################################
# Created by............: Donald Diaz
# Created Date..........: 16th May 2017
# Description...........: Test class
###########################################################################*/
@isTest
public class TestCreateForecastController{

static testMethod void runTest() 
{
    Test.startTest();
    
    Account ac=new Account();
    ac.Name='xyz';
    insert ac;
  
    Deals__c a = new Deals__c();
    a.Exp_of_Accts__c = 1;
    a.Exp_Mkt_Clearing_Price__c = 1;
    a.Total_Installments__c  = 11;
    a.Agency_Cycle__c   = 'Performing';
    a.Asset_Class__c   = 'Credit Card';
    a.Avg_Age_at_Valuation__c   = 2;
    a.Purchase_End_Date__c   =  system.today();
    a.Exp_Face_Value__c   = 1;
    a.General_Notes__c   = 'tested deal';
    a.Bid_IRR__c   = 1;
    a.Product_Type__c   = 'Utility';
    a.Purchase_Start_Date__c   =  System.today();
    a.Company_Name__c= ac.id;
	insert a;
    
    Installment__c inst = new Installment__c();
    inst.Name='abc';
    inst.Deal_Name__c=a.id;
    inst.Est_Close_Date__c=system.today();
    inst.Face_Value__c=1;
    inst.of_Accts__c=1;
    inst.bid_rate__c=1;
    inst.purchase_rate__c=1;
    inst.Asset_Class__c='Other';
    inst.Avg_Age__c=1;
    insert inst;
   
    PageReference p = Page.createforecast;
    p.getParameters().put('id', a.Id);
    Test.setCurrentPage(p);
    
    createforecastcontroller ctrl = new createforecastcontroller(new ApexPages.StandardController(a)); 
    ctrl.forecast();
    Test.stopTest();
       
   list<Forecast__c> b =[SELECT Id,  Forecasted_Avg_Age__c From Forecast__c where Forecasted_Avg_Age__c=: a.Avg_Age_at_Valuation__c];
   System.assertEquals(1, b.size()); 
            
    
  }
 }
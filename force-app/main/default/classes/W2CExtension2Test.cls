@isTest(SeeAllData=true)
public class W2CExtension2Test {
     
    @isTest static void testDoneElse()
    {
        //insert new record without attachment for testing that it gets updated
        Case newCase = new Case();
        	newCase.Auto_Response__c=true;
        	newCase.Origin='Web';
        	newCase.RecordTypeId='012500000009hSj';
        	newCase.SuppliedName='testDoneElse2';
        	newCase.SuppliedEmail='SuppliedEmail@mcmcg.com';
        	newCase.Requested_Due_Date__c=system.today()+1;
        	newCase.OnDemand_Ordered_Date__c=system.today();
        	newCase.Media_Request_Type__c='Other';
			newCase.Customer_Name__c='Customer Name';
        	newCase.W2C_Account_Number_Type__c='MCM';
        	newCase.W2C_Account_Number__c='1234567890';
        	newCase.Company_Purchased_from__c='Company Name';
        	newCase.Original_Account_Number__c='0987654321';
        	newCase.Document_Date__c=system.today();
        	newCase.Media_Document_Type__c='Other/Multiple';
        	newCase.Description='Test';
        upsert newCase;
        id newId = newCase.id;
        //execute test run
         try
            {
                ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
                W2CExtension2 W2CExtension2Obj = new W2CExtension2(sc);//Instantiate the Class
                W2CExtension2Obj.Done();
             }
            catch(Exception ee)
            {}
        //verify the results of test
        list<Case> case1 = [SELECT Id, SuppliedName, Requested_Due_Date__c FROM Case WHERE Requested_Due_Date__c=:system.today()+1 AND SuppliedName='testDoneElse2'];//retrieve the record
        integer i = case1.size();
        system.assertEquals(1,i);
    }

    @isTest static void testAttachIf()
    {
        //insert new record without attachment for testing adding attachment
        Case newCase = new Case();
        	newCase.Auto_Response__c=false;
        	newCase.Origin='Web';
        	newCase.RecordTypeId='012500000009hSj';
        	newCase.SuppliedName='testDoneElse2';
        	newCase.SuppliedEmail='SuppliedEmail@mcmcg.com';
        	newCase.Requested_Due_Date__c=system.today()+1;
        	newCase.OnDemand_Ordered_Date__c=system.today();
        	newCase.Media_Request_Type__c='Other';
			newCase.Customer_Name__c='Customer Name';
        	newCase.W2C_Account_Number_Type__c='MCM';
        	newCase.W2C_Account_Number__c='1234567890';
        	newCase.Company_Purchased_from__c='Company Name';
        	newCase.Original_Account_Number__c='0987654321';
        	newCase.Document_Date__c=system.today();
        	newCase.Media_Document_Type__c='Other/Multiple';
        	newCase.Description='Test';
        upsert newCase;
        id newId = newCase.id;
        Attachment newFile = new Attachment();
        	newFile.Name='TestAttachIfName';
        		Blob bodyBlob=Blob.valueOf('TestAttachIfBody');
        	newFile.Body=bodyBlob;
        	newFile.ParentId=newCase.id;
        insert newFile;
         //execute test run
         try
            {
                ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
                W2CExtension2 W2CExtension2Obj = new W2CExtension2(sc);//Instantiate the Class
                W2CExtension2Obj.Attach();
             }
            catch(Exception ee)
            {}
        //verify the results of test
        list<Attachment> attach1 = [SELECT Id, Name FROM Attachment WHERE parent.id=:newCase.id];//retrieve the record
        integer i = attach1.size();
        system.assertEquals(1,i);
    }
}
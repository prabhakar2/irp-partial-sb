/*
    * @ Trigger Name 	    : 	BackupTrigger
    * @ Description 	    : 	Trigger on Backup__c object.
    * @ Created By		    :   Prabhakar Joshi   
    * @	Created Date	    :   23-Sept-2020
    * @ Last Modified By    :   Shivangi Srivastava
*/

trigger BackupTrigger on Backup__c (before insert, before update, after insert, after update, after delete,after undelete) {
    Boolean triggerActive = Trigger_Setting__c.getValues('BackupTrigger') != NULL ? Trigger_Setting__c.getValues('BackupTrigger').Active__c : false;
    if(triggerActive){
        BackupTriggerHandler handler = new BackupTriggerHandler();
        if(trigger.isBefore){
            if(trigger.isInsert){
                handler.beforeInsert(trigger.New);
            }if(trigger.isUpdate){
                handler.beforeUpdate(trigger.New,trigger.oldMap);
            }
        }
        if(trigger.isAfter){
            if(trigger.isInsert){
                handler.afterInsert(trigger.New); 
            }if(trigger.isUpdate){
                handler.afterUpdate(trigger.New, trigger.oldMap); 
            }if(trigger.isDelete){
                handler.afterDelete(trigger.old);
            }if(trigger.isUndelete){
                handler.afterDelete(trigger.old);
            }
        }
    }
}
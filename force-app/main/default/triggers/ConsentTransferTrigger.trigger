trigger ConsentTransferTrigger on Consent_Transfer__c (after insert , after update) 
{
        Boolean triggerActive = Trigger_Setting__c.getValues('ConsentTransferTrigger') != NULL ? Trigger_Setting__c.getValues('ConsentTransferTrigger').Active__c : false;
        if(triggerActive){
        ConsentTransferTrigger_Helper handler = new ConsentTransferTrigger_Helper();
        if(trigger.isAfter){
            if(trigger.isUpdate)
            {       
             	handler.updateInstallmentConsentCHA(trigger.new,trigger.oldMap);       
            }       
           else if(trigger.isInsert)
            {
                handler.updateInstallmentConsentTransfer(trigger.new);
            }
        }
      }      
}
/***************************************************************
 * Trigger Name : DealTrigger
 * Created By   : Vaibhav Jain  
 * CreatedDate  : 01-July-2020
 * Description  : This trigger is used to cover the work done by process builders.
 * ************************************************************* */
trigger DealTrigger on Deals__c (after update , after insert) 
{
    Boolean triggerActive = Trigger_Setting__c.getValues('DealTrigger') != NULL ? Trigger_Setting__c.getValues('DealTrigger').Active__c : false;
    if(triggerActive){
    DealTrigger_Helper handler = new DealTrigger_Helper();
    if(trigger.isAfter){
        if(trigger.isUpdate){
                /***************** for after update *******************/
                handler.afterUpdateMediaAmt(trigger.new,trigger.oldMap);
                handler.updateInstallmentComments(trigger.new , trigger.oldMap);
                handler.updateInstallmentQuestions(trigger.new , trigger.oldMap);
                handler.updateInstallmentValuationDetails(trigger.new , trigger.oldMap);
                handler.updateInstallmentPutbacks(trigger.new , trigger.oldMap);
                handler.updateInstallmentSLA(trigger.new , trigger.oldMap);
        }
       else if(trigger.isInsert)
        {
            handler.CompanyFamilyToDeal(trigger.new);
        }
    }
  }      
}